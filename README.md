[![OpenAPI Version 3.0](https://img.shields.io/badge/OpenAPI-3.0-blue.svg)](https://swagger.io/specification/)
# Kong API Gateway CE Admin API
Used Swagger to turn this:

https://docs.konghq.com/0.13.x/admin-api

into this:

https://app.swaggerhub.com/apis/JacobAMason/kong-admin

This is pretty much completely untested, but should work fine as long as the API documentation at Kong's website is accurate.

